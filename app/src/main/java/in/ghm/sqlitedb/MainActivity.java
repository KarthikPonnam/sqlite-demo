package in.ghm.sqlitedb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.w3c.dom.Text;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    DatabaseHandler db = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button save_data = (Button) findViewById(R.id.saveButton);
        Button get_data = (Button) findViewById(R.id.getData);
        final TextView display_data = (TextView) findViewById(R.id.display_data);

        YoYo.with(Techniques.RotateInUpLeft)
                .duration(3000)
                .delay(1000)
                .playOn(save_data);


        save_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(MainActivity.this, "Inserting Data...", Toast.LENGTH_SHORT).show();

                /**
                 * CRUD Operations
                 * */
                // Inserting Contacts
                Log.d("Insert: ", "Inserting ..");
                db.addContact(new Contact("Pranay", "+919652290152"));
                db.addContact(new Contact("Srinivas", "+919177806696"));
                db.addContact(new Contact("Tracy", "+919522222222"));
                db.addContact(new Contact("Karthik", "+919949016646"));

                Toast.makeText(MainActivity.this, "Successful...", Toast.LENGTH_SHORT).show();

            }
        });

        get_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(MainActivity.this, "Retriving Data...", Toast.LENGTH_SHORT).show();

                // Reading all contacts
                Log.d("Reading: ", "Reading all contacts..");

                List<Contact> contacts = db.getAllContacts();
                String data = "";
                for (Contact cn : contacts) {

                    String log = "Id: "+cn.getID()+" \nName: " + cn.getName() + " \nPhone: " + cn.getPhoneNumber();

                    // Writing Contacts to log
                    Log.d("Name: ", log);
                    data = data + log + "\n\n\n" ;
                    //Toast.makeText(getApplicationContext(),data, Toast.LENGTH_SHORT).show();

                }

                display_data.setText(data);

            }
        });







    }
}
